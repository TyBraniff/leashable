package com.ranull.leashable.nms;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.entity.Mob;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_18_R1.entity.CraftMob;
import org.bukkit.entity.Hanging;
import org.bukkit.entity.LivingEntity;

public class NMS_v1_18_R1 implements NMS {
    @Override
    public void leash(org.bukkit.entity.Entity entity, org.bukkit.entity.Entity holder) {
        if (entity instanceof org.bukkit.entity.Mob) {
            Mob mob = ((CraftMob) entity).getHandle();
            CompoundTag compoundTag = new CompoundTag();
            CompoundTag leashCompoundTag = new CompoundTag();

            mob.save(compoundTag);

            if (holder instanceof LivingEntity) {
                leashCompoundTag.putUUID("UUID", holder.getUniqueId());
            } else if (holder instanceof Hanging) {
                Location location = holder.getLocation();

                leashCompoundTag.putInt("X", (int) location.getX());
                leashCompoundTag.putInt("Y", (int) location.getY());
                leashCompoundTag.putInt("Z", (int) location.getZ());
            }

            compoundTag.put("Leash", leashCompoundTag);
            compoundTag.putByte("Leashed", (byte) 1);
            compoundTag.putString("world", entity.getWorld().getName());
            mob.load(compoundTag);
        }
    }

    @Override
    public void unleash(org.bukkit.entity.Entity entity) {
        if (entity instanceof org.bukkit.entity.Mob) {
            Mob mob = ((CraftMob) entity).getHandle();
            CompoundTag compoundTag = new CompoundTag();

            compoundTag.put("Leash", new CompoundTag());
            compoundTag.putByte("Leashed", (byte) 0);
            mob.save(compoundTag);
        }
    }

    @Override
    public boolean canHaveLeash(org.bukkit.entity.Entity entity) {
        return entity instanceof org.bukkit.entity.Mob;
    }

    @Override
    public boolean canBeLeashed(org.bukkit.entity.Entity entity) {
        return entity instanceof org.bukkit.entity.Mob && ((CraftMob) entity).getHandle().canBeLeashed(null);
    }

    @Override
    public org.bukkit.entity.Entity getHolder(org.bukkit.entity.Entity entity) {
        if (entity instanceof org.bukkit.entity.Mob) {
            Mob mob = ((CraftMob) entity).getHandle();
            CompoundTag compoundTag = new CompoundTag();

            mob.save(compoundTag);

            if (compoundTag.getAllKeys().contains("Leash")) {
                compoundTag = compoundTag.getCompound("Leash");

                if (compoundTag.hasUUID("UUID")) {
                    return entity.getServer().getEntity(compoundTag.getUUID("UUID"));
                } else if (compoundTag.contains("X") && compoundTag.contains("Y") && compoundTag.contains("Z")) {
                    Location location = new Location(entity.getWorld(), compoundTag.getInt("X"),
                            compoundTag.getInt("Y"), compoundTag.getInt("Z"));

                    for (org.bukkit.entity.Entity nearbyEntity : entity.getWorld()
                            .getNearbyEntities(location, 0.5, 0.5, 0.5)) {
                        if (nearbyEntity instanceof Hanging) {
                            return nearbyEntity;
                        }
                    }
                }
            }
        }

        return null;
    }
}
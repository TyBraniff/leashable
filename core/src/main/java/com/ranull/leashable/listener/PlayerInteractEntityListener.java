package com.ranull.leashable.listener;

import com.ranull.leashable.Leashable;
import org.bukkit.GameMode;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerLeashEntityEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;

public class PlayerInteractEntityListener implements Listener {
    private final Leashable plugin;

    public PlayerInteractEntityListener(Leashable plugin) {
        this.plugin = plugin;
    }

    @SuppressWarnings("deprecation")
    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onPlayerInteractEntity(PlayerInteractEntityEvent event) {
        Player player = event.getPlayer();
        Entity entity = event.getRightClicked();
        ItemStack itemStack = event.getHand() == EquipmentSlot.OFF_HAND
                ? player.getInventory().getItemInOffHand()
                : player.getInventory().getItemInHand();

        if (entity instanceof LivingEntity && ((LivingEntity) entity).hasAI()
                && (player.hasPermission("leashable.leash.*")
                || player.hasPermission("leashable.leash." + entity.getType().name()))
                && itemStack.getType().name().matches("(?i)LEAD|LEASH") && plugin.getNMS().canHaveLeash(entity)
                && !plugin.getNMS().canBeLeashed(entity) && plugin.getNMS().getHolder(entity) == null) {
            PlayerLeashEntityEvent playerLeashEntityEvent = new PlayerLeashEntityEvent(entity, player, player);

            plugin.getServer().getPluginManager().callEvent(playerLeashEntityEvent);

            if (!playerLeashEntityEvent.isCancelled()) {
                if (entity.isInsideVehicle()) {
                    entity.leaveVehicle();
                }

                plugin.getNMS().leash(entity, player);
                event.setCancelled(true);

                if (player.getGameMode() != GameMode.CREATIVE) {
                    if (itemStack.getAmount() > 1) {
                        itemStack.setAmount(itemStack.getAmount() - 1);
                    } else {
                        player.getInventory().remove(itemStack);
                    }
                }
            }
        }
    }
}

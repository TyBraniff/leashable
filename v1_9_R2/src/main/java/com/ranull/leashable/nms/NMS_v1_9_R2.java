package com.ranull.leashable.nms;

import net.minecraft.server.v1_9_R2.EntityHuman;
import net.minecraft.server.v1_9_R2.EntityInsentient;
import net.minecraft.server.v1_9_R2.EntityLiving;
import net.minecraft.server.v1_9_R2.NBTTagCompound;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_9_R2.entity.CraftLivingEntity;
import org.bukkit.entity.Hanging;
import org.bukkit.entity.LivingEntity;

import java.util.UUID;

public class NMS_v1_9_R2 implements NMS {
    @Override
    public void leash(org.bukkit.entity.Entity entity, org.bukkit.entity.Entity holder) {
        if (entity instanceof org.bukkit.entity.LivingEntity) {
            EntityLiving entityLiving = ((CraftLivingEntity) entity).getHandle();

            if (entityLiving instanceof EntityInsentient) {
                EntityInsentient entityInsentient = (EntityInsentient) entityLiving;
                NBTTagCompound nbtTagCompound = new NBTTagCompound();
                NBTTagCompound leashNBTTagCompound = new NBTTagCompound();

                entityInsentient.e(nbtTagCompound);

                if (holder instanceof LivingEntity) {
                    leashNBTTagCompound.a("UUID", holder.getUniqueId());
                } else if (holder instanceof Hanging) {
                    Location location = holder.getLocation();

                    leashNBTTagCompound.setInt("X", (int) location.getX());
                    leashNBTTagCompound.setInt("Y", (int) location.getY());
                    leashNBTTagCompound.setInt("Z", (int) location.getZ());
                }

                nbtTagCompound.set("Leash", leashNBTTagCompound);
                nbtTagCompound.setByte("Leashed", (byte) 1);
                nbtTagCompound.setString("world", entity.getWorld().getName());
                entityInsentient.f(nbtTagCompound);
            }
        }
    }

    @Override
    public void unleash(org.bukkit.entity.Entity entity) {
        if (entity instanceof org.bukkit.entity.LivingEntity) {
            EntityLiving entityLiving = ((CraftLivingEntity) entity).getHandle();

            if (entityLiving instanceof EntityInsentient) {
                EntityInsentient entityInsentient = (EntityInsentient) entityLiving;
                NBTTagCompound nbtTagCompound = new NBTTagCompound();

                nbtTagCompound.set("Leash", new NBTTagCompound());
                nbtTagCompound.setByte("Leashed", (byte) 0);
                entityInsentient.e(nbtTagCompound);
            }
        }
    }

    @Override
    public boolean canHaveLeash(org.bukkit.entity.Entity entity) {
        return entity instanceof org.bukkit.entity.LivingEntity
                && ((CraftLivingEntity) entity).getHandle() instanceof EntityInsentient;
    }

    @Override
    public boolean canBeLeashed(org.bukkit.entity.Entity entity) {
        if (entity instanceof org.bukkit.entity.LivingEntity) {
            EntityLiving entityLiving = ((CraftLivingEntity) entity).getHandle();

            return entityLiving instanceof EntityInsentient && ((EntityInsentient) entityLiving).a((EntityHuman) null);
        }

        return false;
    }

    @Override
    public org.bukkit.entity.Entity getHolder(org.bukkit.entity.Entity entity) {
        if (entity instanceof org.bukkit.entity.LivingEntity) {
            EntityLiving entityLiving = ((CraftLivingEntity) entity).getHandle();

            if (entityLiving instanceof EntityInsentient) {
                EntityInsentient entityInsentient = (EntityInsentient) entityLiving;
                NBTTagCompound nbtTagCompound = new NBTTagCompound();

                entityInsentient.e(nbtTagCompound);

                if (nbtTagCompound.c().contains("Leash")) {
                    nbtTagCompound = nbtTagCompound.getCompound("Leash");

                    if (nbtTagCompound.b("UUID")) {
                        UUID uuid = nbtTagCompound.a("UUID");

                        for (org.bukkit.entity.Entity worldEntity : entity.getWorld().getEntities()) {
                            if (worldEntity.getUniqueId().equals(uuid)) {
                                return worldEntity;
                            }
                        }
                    } else if (nbtTagCompound.hasKey("X") && nbtTagCompound.hasKey("Y")
                            && nbtTagCompound.hasKey("Z")) {
                        Location location = new Location(entity.getWorld(), nbtTagCompound.getInt("X"),
                                nbtTagCompound.getInt("Y"), nbtTagCompound.getInt("Z"));

                        for (org.bukkit.entity.Entity nearbyEntity : entity.getWorld()
                                .getNearbyEntities(location, 0.5, 0.5, 0.5)) {
                            if (nearbyEntity instanceof Hanging) {
                                return nearbyEntity;
                            }
                        }
                    }
                }
            }
        }

        return null;
    }
}